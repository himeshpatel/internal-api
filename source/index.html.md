---
title: Acerta API (Internal)


language_tabs:
  - shell

toc_footers:
  - <a href='#'>Sign Up for a Developer Key</a>
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true
---

# Introduction

Welcome to Acerta Documentation!. Acerta Analytics API provides a restful API that can help you analyze your data using Acerta's complex machine learning algorithms. Our machine learning platform helps users understand their data by reducing resources needed to detect failures and find root cause of the faults. 

The Acerta Analytics API are  used for uploading sensor data from automobiles and automobile sub-systems like gearboxes. Acerta's machine learning platform will run an analysis in real-time and provide results highlighting the anomalous regions. 

All API access is over HTTPS, and accessed from the zf.acerta.ca domain. Our API is designed to use HTTP response codes to indicate API success/errors. JSON will be returned in all responses from the API.

```shell

  █████╗  ██████╗███████╗██████╗ ████████╗ █████╗ ██╗
 ██╔══██╗██╔════╝██╔════╝██╔══██╗╚══██╔══╝██╔══██╗██║
 ███████║██║     █████╗  ██████╔╝   ██║   ███████║██║
 ██╔══██║██║     ██╔══╝  ██╔══██╗   ██║   ██╔══██║╚═╝
 ██║  ██║╚██████╗███████╗██║  ██║   ██║   ██║  ██║██╗
 ╚═╝  ╚═╝ ╚═════╝╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝
                                                                                                                                 
```
# Authentication
In order to use Acertas analytics API, you need a personal API token. An access token is a data string that enables Acerta to verify that a request belongs to an authorized session.

In the normal order of operations users will begin by requesting authentication from Acerta authorized endpoint and Acerta will send users a username and password. Users will then send the username and password to the token endpoint in a request for an access token. Users can then use the returned access token to make Acerta API calls.

> To request access token, use the following endpoint:

###HTTP REQUEST

`GET https://zf.acerta.ca/login/token/<validity>`


```shell
curl -k -u uname:pwd -X GET 
 -L https://zf.acerta.ca/login/token/7200

```
> The above command returns JSON structured like this:

```json
{
     "duration": "7200",
     "token": "eyJhbGciOiJIUzI1NiIsImlhdCI6MTUwMzM1NDgxOCwiZXhwIjoxNTAzMzYyMDE4fQ.eyJ1aWQiOjU4ODh9.R50HR7gfMbemLC5dLXPzxOJXDjL67uBwYwRDnIqkqFE"
}
```

<aside class="notice">You must replace username and password value with one provided by acerta</aside>

Acerta uses Basic Auth over HTTPS for authentication. The -u option specifies user credentials, where uname is the user name and pwd is the current password. These credentials will be provided by acerta on request from users. Unauthenticated requests will return an HTTP 401 response. Acerta analytics API expects for the API key to be included in all API requests to the server. 

Validity of a token is the time till which the token will be valid, after which the token expires and user has to generate a new token. The access token needs to be renewed every 3600 seconds. This is a default option, but it can be changed to increase the time limit. In order to get the tokens using the API, the users are advised to specify a timestamp in seconds for the validity. A request with validity of more then 10 hours i.e 36000 sec will be set to the default value of 3600sec.   

<aside class="success"> Token returned you are authorized to use Acerta API </aside>

Do not forget to copy/paste your API token to access.key file, we don’t store it as plain text!

#Prediction
Users can use Acertas analytics API by uploading traces to the server. These traces will then be analyzed by complex machine learning models to predict failures and anomlies im them.  In order to use this API,  the users will have to specify a valid API key generated from the authorization endpoint above. 

<aside class="notice"> Do not forget to copy/paste your API token to access.key file, we don’t store it as plain text! </aside>

Once the analysis is completed Acerta generates spectral analysis graph of the file. The users are notified of the results via email and are also reflected in the webui.

This command requires the bom, station, access key file, and data file to be specified. The bom and station information can be extracted from file path on the network drive. The the serial number of the gearbox and timestamps, can be extracted from the uploaded filename.

```shell
curl -i -k -v -X POST -H "Content-Type: multipart/form-data" -F bom=34 -F station=1 
  -F 'file=@/Path/To/2015072201_150724203142.tdms' -F 'access=@/Path/To/access.key'  
    https://zf.acerta.ca/uploadLocalData

```

### HTTP Request

POST https://zf.acerta.ca/uploadLocalData

### Request Parameters
<aside class="notice">User must specify bom and station when uploading data</aside>
<aside class="warning">Access Key and password are limited to one user and should not be shared with anyone else</aside>

Parameter | Default | Description
--------- | ------- | -----------
unitid | filename | The unitid name taken from filename
BOM | N/A | The BOM of the unit
Station | N/A | The station number of unit

```json
{
      "status_code": 200,
      "status_txt": "OK",
      "tid": 3526
}

```

# Get all transferred files 
To get list of all files previously uploaded to acerta servers, users can send get request to this endpoint. 

```shell
curl -k -u uname:pwd -i -X GET -L https://zf.acerta.ca/getTransferredList

```
The -u option specifies user credentials,  where uname is the username and pwd is the password provided by acerta on request. If the request is successful a list of names of files uploaded are returned. If the credentials are invalid, 401 is returned by the server. 

```json
[
     {
          "name": "2017081603_170822125433.tdms_index"
     },
     {
          "name": "2017081608_141023114638.tdms"
     },
     {
          "name": "2017071208_141023114638.tdms_index"
     },
     "status_code": 200,
     "status_txt": "OK"
]
```
# AJAX Calls

## Upload data

```javascript
$.ajax({
      type: 'POST',
      url: '../../uploadLocalData',
      data: form_data,
      contentType: false,
      processData: false,
    })
```

> The above command returns JSON structured like this:

```json
{
   'success':True,
   204
}
```

This endpoint uploads a given unit to AWS.

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
unitid | filename | The unitid name taken from filename
BOM | N/A | The BOM of the unit
Station | N/A | The station number of unit

### HTTP Request

`GET http://example.com/uploadLocalData/123`

## Upload Notes

```javascript
$.ajax({
      type: 'POST',
      url: '../../uploadNotes',
      data: form_data,
      contentType: false,
      processData: false,
    })
```

> The above command returns JSON structured like this:

```json
{
   'success':True,
   204
}
```

This endpoint uploads a given unit to AWS.

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
unitid | filename | The unitid is needed to change the engineering notes
inspection_status | N/A | The inspection status that will be changed after submisison
notes | N/A | The engineering notes that are submitted and saved

### HTTP Request

`GET http://example.com/uploadNotes`

## Pagination

```javascript
$.ajax({
      type: 'GET',
      url: '../../pagination',
      data: { pageNum: page }
    })
```

> The above command returns JSON structured like this:

```json
{
   'success':True,
   204
}
```

This endpoint retrieves the given page for the history table given the page number.

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
pagenum | 0 | The page number requested
startdate | -30d | The start date of the date filter
enddate | Today | The end date of the date filter

### HTTP Request

`GET http://example.com/uploadNotes`
