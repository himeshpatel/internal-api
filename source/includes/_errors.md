# Errors
The Acerta API call returns an HTTP status code that reflects the nature of the response. We have done our best to follow the HTTP status code conventions.
Generally the following pattern applies:

2xx - Acerta received, understood, and accepted a request.
3xx - The user agent must take further action in order to complete the request.
4xx - An error occurred in handling the request. The most common cause of this error is an invalid parameter.
5xx - Acerta received and accepted the request, but an error occurred in the Acerta service while handling it.

<aside class="notice">The return response code will differ for each API and will be one from the one in the table</aside>


> The Acerta API uses the following error codes:

```shell
Error Code | Meaning
---------- | -------

400 | Bad Request -- Your request is incorrect
401 | Unauthorized -- Your API key is wrong
403 | Forbidden -- You do not have the correct priviledges to access this content
404 | Not Found -- The specified item could not be found
406 | Not Acceptable -- You requested a format that is not json
500 | Internal Server Error -- We had a problem with our server. Try again later.
503 | Service Unavailable -- We are temporarily offline for maintenance. Please try again later.
```
